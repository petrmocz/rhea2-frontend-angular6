UI for RHEA2 application
========================

Description
-----------
Simple user friendly UI to interact with business processes  modeled and running on Activiti. Require backend (https://gitlab.com/petrmocz/rhea2).

Still under development

![Home screen screenshot](./doc/assets/Screenshot_2018-10-02 Rhea2FrontendAngular6.png)

Uses
----
 - Angular6,
 - Bootstrap & FontAwersome. Thanks to @beeman [https://medium.com/@beeman/tutorial-styling-angular-cli-v6-apps-with-bootstrap-8d4f8ea5adae]
 -
