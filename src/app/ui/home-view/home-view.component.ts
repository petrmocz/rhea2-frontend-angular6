import { RheaService } from './../../services/rhea/rhea.service';
import { Component, OnInit } from '@angular/core';
import { Process } from '../../model/process';
import { ProcessStep } from './../../model/process-step';

import { Location } from '@angular/common';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.css']
})
export class HomeViewComponent implements OnInit {

  public processes: Process[];

  constructor(private service: RheaService, private location: Location) { }

  ngOnInit() {
    this.service.getProcesses().subscribe((data: Process[]) => {
      this.processes = data;
    });
  }

}
