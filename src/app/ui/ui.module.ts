import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProcessListComponent } from './process-list/process-list.component';
import { TaskListComponent } from './task-list/task-list.component';
import { ProcessStepComponent } from './process-step/process-step.component';
import { ProcessViewComponent } from './process-view/process-view.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { ProcessStepItemComponent } from './process-step-item/process-step-item.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    ProcessListComponent,
    TaskListComponent,
    ProcessStepComponent,
    ProcessViewComponent,
    HomeViewComponent,
    ProcessStepItemComponent,
    SettingsComponent
  ],
  exports: [
    LayoutComponent,
    HomeViewComponent,
    ProcessViewComponent,
  ]
})
export class UiModule { }
