import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessStepItemComponent } from './process-step-item.component';

describe('ProcessStepItemComponent', () => {
  let component: ProcessStepItemComponent;
  let fixture: ComponentFixture<ProcessStepItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessStepItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessStepItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
