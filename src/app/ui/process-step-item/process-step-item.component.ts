import { ProcessStepItem } from './../../model/process-step-item';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-process-step-item',
  templateUrl: './process-step-item.component.html',
  styleUrls: ['./process-step-item.component.css']
})
export class ProcessStepItemComponent implements OnInit {

  @Input() item: ProcessStepItem;

  constructor() { }

  ngOnInit() {
  }

}
