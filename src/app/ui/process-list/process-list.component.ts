import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Process } from '../../model/process';
import { RheaService } from '../../services/rhea/rhea.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';


@Component({
  selector: 'app-process-list',
  templateUrl: './process-list.component.html',
  styleUrls: ['./process-list.component.css']
})
export class ProcessListComponent implements OnInit {

  @Input() myProcesses: Process[];
  selectedProcess: Process;
  @Output() processChanged = new EventEmitter<Process>();

  constructor(private service: RheaService, private titleService: Title, private router: Router) { }

  ngOnInit() {
    this.service.getProcesses().subscribe((data: Process[]) => {
      this.myProcesses = data;
    });
  }

  selectProcess(process: Process) {

    this.selectedProcess = process;
    this.titleService.setTitle(process.name);
    this.router.navigate(['/process/', process.id]);
    return false;
  }

}
