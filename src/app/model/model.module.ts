import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from './user';
import { Process } from './process';
import { ProcessStep } from './process-step';
import { ProcessStepItem } from './process-step-item';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    Process,
    ProcessStep,
    ProcessStepItem,
    User],
  exports: [Process, ProcessStep, ProcessStepItem, User]
})
export class ModelModule { }
