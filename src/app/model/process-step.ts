import { ProcessStepItem } from './process-step-item';

export class ProcessStep {
    name: string;
    items: ProcessStepItem[];
}
