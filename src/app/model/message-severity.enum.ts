export enum MessageSeverity {
    DEBUG,
    INFO,
    SUCCESS,
    WARNING,
    ERROR
}
