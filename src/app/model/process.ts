import { ProcessStep } from './process-step';

export class Process {
    // private _id: number;     // processInstance id
    // private _name: string;   // processInstance name
    // private _steps: ProcessStep[]; // process instance steps

    constructor(private _id: number, private _name: string, private _desc: string, private _image: string, private _steps: ProcessStep[]) {
        // this._id = _id;
        // this._name = _name;
        // this._desc = _desc;
        // this._image = _image;
        // this._steps = _steps;
    }

    get id(): number { return this._id; }

    get name(): string { return this._name; }

    get desc(): string { return this._desc; }

    get image(): string { return this._image; }

    get steps(): ProcessStep[] { return this._steps; }
}
