import { MessageSeverity } from './message-severity.enum';

export class MessageItem {
    message: string;
    severity: MessageSeverity;
}
