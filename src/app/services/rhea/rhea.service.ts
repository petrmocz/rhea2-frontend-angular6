import { Injectable } from '@angular/core';
import { Process } from '../../model/process';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RheaService {

  URL: string;

  constructor(private http: HttpClient) {
    this.URL = 'http://localhost:9999/api/v1';
  }


  getProcesses() {
    const r = this.http.get(this.URL + '/processes');
    return r;
  }
}
