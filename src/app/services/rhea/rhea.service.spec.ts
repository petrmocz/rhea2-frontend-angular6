import { TestBed } from '@angular/core/testing';

import { RheaService } from './rhea.service';

describe('RheaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RheaService = TestBed.get(RheaService);
    expect(service).toBeTruthy();
  });
});
