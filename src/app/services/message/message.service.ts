import { MessageItem } from './../../model/message-item';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: MessageItem[];

  add(message: MessageItem) {
    this.messages.push(message);
  }

  remove() {
    this.messages.pop();
  }

  clear() {
    this.messages = [];
  }
}
