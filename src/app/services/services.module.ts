import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RheaService } from './rhea/rhea.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RheaService
  ]
})
export class ServicesModule { }
