/*
 * Sample data node.js server
 */
var express = require('express');
var app = express();
var fs = require('fs');

const SAMPLES = "sample-data.json"; // filename witch sample data
const API = "/api/v1"; // API base url
const MAGIC = 1; // magic number to map to javascript array index

// 
app.get(API + "/echo", function (request, response) {
  response.end("Server alive");
});

// query fo processes list
app.get(API + "/processes", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

// query for individual process details 
app.get(API + "/processes/:process_id", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes[request.params.process_id - MAGIC]);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

// query for process steps
app.get(API + "/processes/:process_id/steps", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes[request.params.process_id - MAGIC].steps);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

// query for individual process step details
app.get(API + "/processes/:process_id/steps/:step_id", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes[request.params.process_id - MAGIC].steps[request.params.step_id - MAGIC]);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

// query for items 
app.get(API + "/processes/:process_id/steps/:step_id/items", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes[request.params.process_id - MAGIC].steps[request.params.step_id - MAGIC].items);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

// query for individual process step item details
app.get(API + "/processes/:process_id/steps/:step_id/items/:item_id", function (request, response) {
  fs.readFile(__dirname + "/" + SAMPLES, "utf8", function (err, data) {
    json = JSON.parse(data);
    var returndata = JSON.stringify(json.processes[request.params.process_id - MAGIC].steps[request.params.step_id - MAGIC].items[request.params.item_id - MAGIC]);
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.end(returndata);
  })
});

var server = app.listen(9999, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Sample data server listen at http://%s:%s", host, port);
});
